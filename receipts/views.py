from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required(login_url="/accounts/login/")
def home(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "home": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required(login_url="/accounts/login/")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required(login_url="/accounts/login/")
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": categories}
    return render(request, "receipts/category.html", context)


@login_required(login_url="/accounts/login/")
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}
    return render(request, "receipts/account.html", context)


@login_required(login_url="/accounts/login")
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm

    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required(login_url="/accounts/login")
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm

    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
